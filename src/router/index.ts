import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import Default from '@/layout/Default.vue';
import pageNames from '@/router/pageNames';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: pageNames.DEFAULT,
    component: Default,
    redirect: { name: pageNames.ABOUT_ME },
    children: [
      {
        path: 'about',
        name: pageNames.ABOUT_ME,
        component: () => import('@/pages/AboutMe.vue'),
      },
      {
        path: 'projects',
        name: pageNames.MY_PROJECTS,
        component: () => import('@/pages/Projects.vue'),
      },
      {
        path: 'contacts',
        name: pageNames.CONTACTS,
        component: () => import('@/pages/Contacts.vue'),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
