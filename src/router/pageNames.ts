export default {
  DEFAULT: 'Default',
  ABOUT_ME: 'AboutMe',
  MY_PROJECTS: 'MyProjects',
  CONTACTS: 'Contacts',
};
