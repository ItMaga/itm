import { shallowMount } from '@vue/test-utils';
import Menu from '@/components/layout/header/components/Menu.vue';

describe('Menu.vue', () => {
  it('В Menu есть массив параметров', () => {
    const wrapper = shallowMount(Menu);
    console.log(typeof wrapper.vm.items);
    expect(wrapper.vm.items).toHaveLength(3);
  });
});
