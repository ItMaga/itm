module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: '@import "~@/assets/styles/_variables.scss";',
      },
    },
  },

  pluginOptions: {
    i18n: {
      locale: 'ru',
      fallbackLocale: 'ru',
      localeDir: 'locales',
      enableLegacy: false,
      runtimeOnly: false,
      compositionOnly: false,
      fullInstall: true,
    },
  },
};
